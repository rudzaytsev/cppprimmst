/* 
 * File:   graphtest.cpp
 * Author: rudolph
 *
 * Created on 28.11.2013, 0:13:08
 */

#include <stdlib.h>
#include <iostream>

/*
 * Simple C++ Test Suite
 */

void test1() {
    std::cout << "graphtest test 1" << std::endl;
}

void test2() {
    std::cout << "graphtest test 2" << std::endl;
    std::cout << "%TEST_FAILED% time=0 testname=test2 (graphtest) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% graphtest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% test1 (graphtest)" << std::endl;
    test1();
    std::cout << "%TEST_FINISHED% time=0 test1 (graphtest)" << std::endl;

    std::cout << "%TEST_STARTED% test2 (graphtest)\n" << std::endl;
    test2();
    std::cout << "%TEST_FINISHED% time=0 test2 (graphtest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

