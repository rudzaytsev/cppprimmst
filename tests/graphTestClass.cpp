/*
 * File:   graphTestClass.cpp
 * Author: rudolph
 *
 * Created on 28.11.2013, 0:15:28
 */

#include "graphTestClass.h"
#include "..//Graph.h"


CPPUNIT_TEST_SUITE_REGISTRATION(graphTestClass);

graphTestClass::graphTestClass() {
}

graphTestClass::~graphTestClass() {
}

void graphTestClass::setUp() {
}

void graphTestClass::tearDown() {
}

void graphTestClass::testMethod() {
    
    cout << "\n test graph" << endl;
    Graph g(5);
    g.setWeight(0,1,1);
    g.setWeight(0,2,2);
    g.setWeight(1,2,20);
    g.setWeight(1,3,3);
    g.setWeight(1,4,4);
    g.setWeight(3,4,22);
    g.setWeight(2,4,24);
    
    g.print();
    cout << " value = ";
    cout << g.getWeight(0,0) << endl;
         
    
}

void graphTestClass::testFailedMethod() {
    //CPPUNIT_ASSERT(false);
}

