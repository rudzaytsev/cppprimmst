/*
 * File:   graphTestClass.h
 * Author: rudolph
 *
 * Created on 28.11.2013, 0:15:28
 */

#ifndef GRAPHTESTCLASS_H
#define	GRAPHTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class graphTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(graphTestClass);

    CPPUNIT_TEST(testMethod);
    CPPUNIT_TEST(testFailedMethod);

    CPPUNIT_TEST_SUITE_END();

public:
    graphTestClass();
    virtual ~graphTestClass();
    void setUp();
    void tearDown();

private:
    void testMethod();
    void testFailedMethod();
};

#endif	/* GRAPHTESTCLASS_H */

