/* 
 * File:   MST.h
 * Author: rudolph
 *
 * Created on 28 Ноябрь 2013 г., 19:54
 */
#include "Graph.h"
#include <vector>
#include <iostream>
#include <fstream>


#ifndef MST_H
#define	MST_H


using namespace std;

int calcWeightMST( vector<int> &parrent, Graph &g );
void printMstEdges( vector<int> &parrent );
void prim (Graph &g,int n);
void generateBigTestGraphFile(fstream &file,int totalVerts);



#endif	/* MST_H */

