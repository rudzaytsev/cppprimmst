#include "MST.h"



void prim (Graph &g,int n){
        
    vector<bool> inMst (n);
    vector<int> minEdge (n, Graph::INF);
    vector<int> parrent (n, -1);
    minEdge[0] = 0;
    
    for(int i = 0; i < n; ++i) {
        
	int v = -1;
	for (int j = 0; j < n; ++j){
            
		if (!inMst[j] &&
                    (v == -1 || minEdge[j] < minEdge[v])){
			v = j;
                }
        }
	if (minEdge[v] == Graph::INF) {
		cout << "No MST!";
		return;
	}
 
	inMst[v] = true;
	if (parrent[v] != -1){
		//cout << v << " -> " << parrent[v] << endl;
        }
         
	for (int to = 0; to < n; ++to){
           // cout << "INSIDE 1" << endl;
           // cout << " v = " << v << " to= " << to << endl;
           // cout << "rows size = " << g.getSizeRows() << endl;
           // cout << "cols size = " << g.getSizeCols() << endl;
		if (g.getWeight(v,to) < minEdge[to]) {
                    //cout << "INSIDE 2" << endl;
			minEdge[to] = g.getWeight(v,to);
			parrent[to] = v;
		}
        }       
    }
    
    cout << "mst weight = " << calcWeightMST(parrent, g) << endl;
    //printMstEdges(parrent);

        
}


int calcWeightMST( vector<int> &parrent, Graph &g ){
  int mstWeight = 0;
    for(int i = 1; i < g.N; i++){
        int addition = g.getWeight(i,parrent[i] );
        if(addition == g.INF){
            return 0;
        }
        mstWeight += addition;
    }
    
    return mstWeight;
}

void printMstEdges( vector<int> &parrent){

    cout << "mst edges [ ";
    for(int i = 1; i < parrent.size(); i++){
        if(i == 1){
            cout << "(" << parrent[i] << "," << i << ")";
        }
        else {
            cout << ", " << "(" << parrent[i] << "," << i << ")";
        }
    }
    cout << " ]" << endl;
}

void generateBigTestGraphFile(fstream &file,int totalVerts){
    
    for(int i = 0; i < totalVerts; i++){
        
        if((2*i + 1) >= totalVerts) {
            break;      // tree constracted
        } 
        file << i << " " << 2*i + 1 << " " << (i+1)*(i+1) << endl;
        file << i << " " << 2*i + 2 << " " << (i+2)*(i+1) << endl;
    }
    
    for(int i = 0; i < totalVerts; i++){
        if((2*i + 1) >= totalVerts){
            break;
        }
        file << 2*i+1 << " " << 2*i+2 << " " << 11*(i+1) << endl;
    }
    
}
            





