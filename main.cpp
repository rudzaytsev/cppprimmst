/* 
 * File:   main.cpp
 * Author: rudolph
 *
 * Created on 27 Ноябрь 2013 г., 17:29
 */

//#include <iostream>
#include <vector>
#include <pthread.h>
#include <time.h>

#include "Graph.h"
#include "MST.h"

using namespace std;

fstream testFile;
Graph graph(testFile,"test02.txt");

vector<bool> inMst (graph.N);
vector<int> minEdge (graph.N, Graph::INF);
vector<int> parrent (graph.N, -1);

vector<int> foundVerts;
    
volatile int k = 0;

int myArray[15] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

pthread_mutex_t mutex;

struct Param {
    int thread_num;
    int total_threads;
};


void* print_key(void *params){
    Param *p = ((Param *) params);
    
    //cout << p->thread_num << endl;
    
    int threads = p->total_threads;
    int verts = 15;
    int cur_thread_num = p->thread_num;
    int start = (cur_thread_num - 1) * ( verts / threads);
    int finish = cur_thread_num * (verts / threads);
    
    if(cur_thread_num == threads ){
        finish = verts;
    }
       
    pthread_mutex_lock(&mutex);
   
    cout <<"[" << start << " " << finish << "]" << endl;  
    cout << endl;
    
    pthread_mutex_unlock(&mutex);
    
    return NULL;    
}





void* findMinEdge(void *params){
    
    Param *p = ((Param *) params);
    
    int threadNum = p->thread_num;
    int totalVerts = graph.N;
    int totalThreads = p->total_threads;
    
    int cur_thread_num = p->thread_num;
    int start = (cur_thread_num - 1) * ( totalVerts / totalThreads);
    int finish = cur_thread_num * (totalVerts / totalThreads);
    if(cur_thread_num == totalThreads ){
        finish = totalVerts;
    }
    
       
    int v = -1;
    for (int j = start; j < finish; ++j){

            if (!inMst[j] &&
                (v == -1 || minEdge[j] < minEdge[v])){
                    v = j;
            }
    }
 
     
    pthread_mutex_lock(&mutex);     
   /*
    cout << "[" << cur_thread_num << "]" << " => "
         <<  "{" << v << " e = " << minEdge[v] << "}" << endl; 
    */    
    
    // return candidate vert which probably has min edge to MST
    foundVerts.push_back(v);
    pthread_mutex_unlock(&mutex);        
        
    return NULL;
}




void parallelMst(int numThreads ){
    
    minEdge[0] = 0;
    
   // cout << "Start all threads" << endl;
    
    pthread_t *ptr_thread_id = new pthread_t[numThreads];
    Param *ptr_params = new Param[numThreads];
    
    for(int j= 0; j < graph.N;j++){
    
    for(int i = 0; i < numThreads; i++){
        
        ptr_params[i].thread_num = i+1;
        ptr_params[i].total_threads = numThreads;
        pthread_create(&ptr_thread_id[i],NULL,&findMinEdge,&ptr_params[i]);
    }

        
    for(int i = 0; i < numThreads; i++){
        
        pthread_join(ptr_thread_id[i], NULL);
        
    }
    
    int minE = graph.INF;
    int vert  = -1; 
    
    for(int i = 0; i < numThreads; i++){
        if(foundVerts.at(i) < 0){
            continue;
        }
        if(minEdge[foundVerts.at(i)] < minE){
            minE = minEdge[foundVerts.at(i)];
            vert = foundVerts.at(i);
        }
    }
    
    if(vert < 0){ 
        cout << "Vert Error! No MST!!!" << endl;
        return;
    }
    
    inMst[vert] = true;
    
   // cout << "Added in Mst vert = " << vert << endl;
    for (int to = 0; to < graph.N; ++to){
           // cout << "INSIDE 1" << endl;
           // cout << " v = " << v << " to= " << to << endl;
           // cout << "rows size = " << g.getSizeRows() << endl;
           // cout << "cols size = " << g.getSizeCols() << endl;
        if (graph.getWeight(vert,to) < minEdge[to]) {
            //cout << "INSIDE 2" << endl;
	    minEdge[to] = graph.getWeight(vert,to);
            parrent[to] = vert;
	}
     }       
    
     foundVerts.clear();   

    }// end for( j)

    //cout << " End all threads " << endl;
    int mstWeight = calcWeightMST(parrent,graph);
    cout << " Mst weight = " << mstWeight << endl;
   // printMstEdges(parrent);


}


int main(int argc, char** argv) {
    
      
    
    /*
    fstream testfile;
    testfile.open("test02.txt",fstream::out);
    if(testfile.is_open()){
        generateBigTestGraphFile(testfile,1000);
        testfile.close();
    }
    else{
        cout << "Error can not open file" << endl;
    }
    */
    
    
    
    clock_t time;
      
    
    cout <<  "*********** PARALLEL PRIM *************" << endl;
    time = clock();
    parallelMst(100);
    time = clock() - time;
    cout << "PARALELL PRIM TIME = " << ((double) time ) / CLOCKS_PER_SEC << " seconds" << endl;
    cout << "PARALELL PRIM TIME = " << ((double) time ) << " ticks" << endl;

    
    cout << "********** SEQ PRIM ********************" << endl;
    clock_t time2;
    time2 = clock();
    prim(graph,graph.N);
    time2 = clock() - time2;
    cout << "SEQ PRIM TIME = " << ((double) time2 ) / CLOCKS_PER_SEC << " seconds" << endl;
    cout << "SEQ PRIM TIME = " << ((double) time2 )  << " ticks" << endl;

     

    
  
     
               
    
    return 0;
}

