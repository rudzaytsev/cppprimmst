/* 
 * File:   Graph.cpp
 * Author: rudolph
 * 
 * Created on 27 Ноябрь 2013 г., 23:34
 */

#include "Graph.h"
#include "Edge.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

Graph::Graph() {
}

Graph::Graph(const Graph& orig) {
}

Graph::~Graph() {
}

Graph::Graph(int n){
    
    N = n;
    for(int i = 0; i < N; i++){
        
        adjVerts v;
        for(int j = 0; j < N; j++){
            v.push_back(1000000);
        }
        verts.push_back(v);        
    }
}

Graph::Graph(fstream &file,const char* filename){
    
    file.open(filename,ios::in);
    
    if(file.is_open()){
        
        vector<Edge> edges;
        int v1 = 0;
        int v2 = 0;
        int w = 0;
        
        while(!file.eof()){
                file >> v1;
                file >> v2;
                file >> w;
                // петли и нулевые и отрицательные веса запрещены
                if((w <= 0) || (v1 == v2)){
                    w = 1000000; //INF
                }
                Edge edge(v1, v2, w);
                edges.push_back(edge);
                
               // cout << "readline" << endl;
        }
        
        int maxVertNum = 0;
        for(int i = 0; i < edges.size(); i++){
            int a = edges.at(i).getFirstVert();
            int b = edges.at(i).getSecondVert();
            if(a > maxVertNum){
                maxVertNum = a;
            }
            if(b > maxVertNum ){
                maxVertNum = b;
            }
        }
        
        cout << "maxVertNum = " << maxVertNum << endl;
        N = maxVertNum + 1;
        for(int i = 0; i < N; i++){
        
            adjVerts v;
            for(int j = 0; j < N; j++){
                v.push_back(1000000);
            }
            verts.push_back(v);        
        }
        
        for(int i = 0; i < N; i++){
            this->setWeight(edges.at(i).getFirstVert(),
                            edges.at(i).getSecondVert(),
                            edges.at(i).getWeight());
        }
        
    }
    else {
        cout << "Can not open file " << endl;
    }
        
    
}



void Graph::setWeight(int from, int to, int weight){
    
    verts.at(from).at(to) = weight;
    verts.at(to).at(from) = weight;
}

int Graph::getWeight(int from,int to){
    return verts.at(from).at(to);
}

void Graph::print(){
      
    for(int i = 0; i < N; i++){
        
        cout << "[" << i << "]" << " -> { ";
        
        for(int j = 0; j < N; j++){
            
            int val = verts.at(i).at(j);
            if(j == 0){
                if(val != INF){
                     cout << val;
                }
                else { cout << "INF"; }
            }
            else {
                if(val != INF){
                      cout << ", " << val;
                }
                else { cout << ", " << "INF"; }
            }
        }
        cout << "}" << endl;
    } 
    
}

int Graph::getSizeRows(){
    return verts.size();
}

int Graph::getSizeCols(){
    return verts.at(0).size();
}





